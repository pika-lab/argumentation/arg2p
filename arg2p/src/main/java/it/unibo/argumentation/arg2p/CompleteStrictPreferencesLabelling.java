package it.unibo.argumentation.arg2p;

public class CompleteStrictPreferencesLabelling extends Arg2PLibrary {
    public CompleteStrictPreferencesLabelling() {
        super("core/labellings/argument/complete");
    }
}
