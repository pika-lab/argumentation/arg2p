package it.unibo.argumentation.arg2p;

public class GroundedStrictPreferencesLabelling extends Arg2PLibrary {
    public GroundedStrictPreferencesLabelling() {
        super("core/labellings/argument/grounded");
    }
}
