package it.unibo.argumentation.arg2p;

public class GroundedDefeasiblePreferencesLabelling extends Arg2PLibrary {
    public GroundedDefeasiblePreferencesLabelling() {
        super("core/labellings/argument/groundedDefeasiblePreferences");
    }
}
